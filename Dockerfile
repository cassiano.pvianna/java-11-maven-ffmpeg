FROM openjdk:11-jdk-slim

WORKDIR /app

ARG MAVEN_VERSION=3.8.3
ARG MVN_FILE=apache-maven-${MAVEN_VERSION}-bin.tar.gz

RUN	apt-get update
RUN	apt-get install apt-transport-https -y
RUN	apt-get install python -y
RUN	apt-get install wget -y
RUN	apt-get install ffmpeg -y

RUN wget https://dlcdn.apache.org/maven/maven-3/${MAVEN_VERSION}/binaries/apache-maven-${MAVEN_VERSION}-bin.tar.gz
RUN tar -xzf ${MVN_FILE}
RUN mv apache-maven-${MAVEN_VERSION} /usr/share/maven
RUN ls -a
RUN ln -s /usr/share/maven/bin/mvn /usr/bin/mvn
RUN rm ${MVN_FILE}  
